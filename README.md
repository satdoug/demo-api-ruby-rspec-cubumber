# Automação de Testes em API com HTTParty e Cucumber

## Sobre o Projeto
Este projeto trata-se de uma demonstração do uso do HTTParty para realizar testes automatizados dentro do processo do BDD utilizando o Cucumber.

## cucumber

#### Pré-requisitos

* Possuir ruby instalado **>= 2.6.6**
* Possuir cucumber instalado **>= 4.1.0**
* Possuir HTTParty instalado **>= 0.18.1**
* Possuir rspec instalado **>= 3.9.0**

#### Instalando dependências do Ruby
* instalar a gem bundler 
```shell
$ gem install bundler
```
* Após instalação, na raiz do projeto, rodar o comando:
 
```shell
$ bundle install
```
#### Executando o cucumber
1. Execute o comando:
```shell
$ cucumber
```